switch plt
    case 0
        % no plotting
    case 1
        t1=sgtitle(title_text);
        set(t1,'Interpreter','latex','FontSize',16);
        
        subplot(1,2,1)
        trisurf(Elements_plot,x_new(:,1),x_new(:,2),x_new(:,3),0)
        axis('equal');
        t1=zlabel(['time $t=$',num2str(t_new)]); % $\nu$ and $H$
        set(t1,'Interpreter','latex','FontSize',12,'HorizontalAlignment', 'left');
        xlim(limits(1,:)); ylim(limits(2,:)); zlim(limits(3,:)); 
        t1=title('surface evolution');
        set(t1,'Interpreter','latex','FontSize',16);
        view(view_angle)
        
        subplot(1,2,2)
        trisurf(Elements_plot,x_new(:,1),x_new(:,2),x_new(:,3),full(u_new(:,4)))
%         shading('interp');
        axis('equal'); colorbar;
        hold on;
        quiver3(x_new(:,1),x_new(:,2),x_new(:,3),u_new(:,1),u_new(:,2),u_new(:,3))
        hold off;
        axis('equal');
        xlim(limits(1,:)); ylim(limits(2,:)); zlim(limits(3,:)); 
        t1=title('$\nu_h$ and $H_h$');
        set(t1,'Interpreter','latex','FontSize',16);
        view(view_angle)

        drawnow
    case 2
        t1=sgtitle(title_text);
        set(t1,'Interpreter','latex','FontSize',15);
        
        subplot(1,2,1)
        
        x_new = real(x_new);
        
        trisurf(Elements_plot,x_new(:,1),x_new(:,2),x_new(:,3),0)
        axis('equal');
        t1=zlabel(['time $t=$',num2str(t_new)]); % $\nu$ and $H$
        set(t1,'Interpreter','latex','FontSize',12,'HorizontalAlignment', 'left');
        xlim(limits(1,:)); ylim(limits(2,:)); zlim(limits(3,:)); 
        t1=title('surface evolution');
        set(t1,'Interpreter','latex','FontSize',15);
        view(view_angle)
        
        subplot(1,2,2)
        u_new(:,4) = real(u_new(:,4));
        trisurf(Elements_plot,x_new(:,1),x_new(:,2),x_new(:,3),full(u_new(:,4)))
%         shading('interp');
        axis('equal'); colorbar;
        hold on;
        quiver3(x_new(:,1),x_new(:,2),x_new(:,3),u_new(:,1),u_new(:,2),u_new(:,3))
        hold off;
        axis('equal');
        xlim(limits(1,:)); ylim(limits(2,:)); zlim(limits(3,:)); 
        t1=title('$\nu$ and $H$');
        set(t1,'Interpreter','latex','FontSize',15);
        view(view_angle)
        
        drawnow
        
        writeVideo(vidObj, getframe(fig));
        
%         if min(abs(t_new-[5 6 7 8])) < 10^-10
%             saveas(fig,['figures_forcedMCF/forcedMCF_gamma',num2str(parameters(6)),'_',num2str(t_new),'_',num2str(n_now),'.fig']);
%             saveas(fig,['figures_forcedMCF/forcedMCF_gamma',num2str(parameters(6)),'_',num2str(t_new),'_',num2str(n_now),'.eps'], 'epsc2');
%         end
    case 3
        t1=sgtitle(title_text);
        set(t1,'Interpreter','latex','FontSize',16);
        
        subplot(2,3,[1 4])
        trisurf(Elements_plot,x_new(:,1),x_new(:,2),x_new(:,3),full(u_new(:,4)))
        colorbar;
        hold on;
        quiver3(x_new(:,1),x_new(:,2),x_new(:,3),u_new(:,1),u_new(:,2),u_new(:,3))
        hold off;
        axis('equal');
        xlim(limits(1,:)); ylim(limits(2,:)); zlim(limits(3,:)); 
        t1=title('$\nu_h$ and $H_h$');
        set(t1,'Interpreter','latex','FontSize',16);
        view(view_angle)
        
        subplot(2,3,[2 5])
        trisurf(Elements_plot,x_new(:,1),x_new(:,2),x_new(:,3),full(w_new(:,4)))
        colorbar;
        hold on;
        quiver3(x_new(:,1),x_new(:,2),x_new(:,3),w_new(:,1),w_new(:,2),w_new(:,3))
        hold off;
        axis('equal');
        xlim(limits(1,:)); ylim(limits(2,:)); zlim(limits(3,:)); 
        t1=title('$z_h$ and $V_h$');
        set(t1,'Interpreter','latex','FontSize',16);
        view(view_angle)
        
        subplot(2,3,3)
        plot(t,norms_nu(1,:),'*- black')
        hold on;
        plot(t,norms_nu(2,:),'o- black')
        hold off;
        ylim([0 2]);
        t1=legend('$\max\{|\nu_h|\}$','$\min\{|\nu_h|\}$');
        set(t1,'Interpreter','latex');
        t1=title('length of $\nu_h$');
        set(t1,'Interpreter','latex','FontSize',16);
        t1=xlabel('time $t$');
        set(t1,'Interpreter','latex','FontSize',12);
        
        
        subplot(2,3,6)
        plot(t,radii_x(1,:),'*- blue')
        hold on;
        plot(t,radii_x(2,:),'o- blue')
        hold off;
        ylim([0 2]);
        t1=legend('$\max\{|X_h|\}$','$\min\{|X_h|\}$');
        set(t1,'Interpreter','latex');
        t1=title('roundness of $X_h$');
        set(t1,'Interpreter','latex','FontSize',16);
        t1=xlabel('time $t$');
        set(t1,'Interpreter','latex','FontSize',12);
        
        drawnow
%         waitforbuttonpress
    case 4
        t1=sgtitle(title_text);
        set(t1,'Interpreter','latex','FontSize',16);
        
        subplot(1,3,1)
        plot(t,norms_nu(1,:),'*- black')
        hold on;
        plot(t,norms_nu(2,:),'o- black')
        hold off;
        ylim([0 2]);
%         ylim([0.8 1.2]);
        t1=legend('$\max\{|\nu_h|\}$','$\min\{|\nu_h|\}$');
        set(t1,'Interpreter','latex','FontSize',13);
        t1=title('length of $\nu_h$');
        set(t1,'Interpreter','latex','FontSize',16);
        t1=xlabel('time $t$');
        set(t1,'Interpreter','latex','FontSize',12);
        
        subplot(1,3,2)
        plot(t,radii_x(1,:),'*- blue')
        hold on;
        plot(t,radii_x(2,:),'o- blue')
        hold off;
        ylim([0 2]);
%         ylim([0.95 1.05]);
        t1=legend('$\max\{|X_h|\}$','$\min\{|X_h|\}$');
        set(t1,'Interpreter','latex','FontSize',13);
        t1=title('roundness of $X_h$');
        set(t1,'Interpreter','latex','FontSize',16);
        t1=xlabel('time $t$');
        set(t1,'Interpreter','latex','FontSize',12);
        
        subplot(1,3,3)
        plot(t,Willmore_energy_vect,'s- red')
        hold on;
        plot(t,W_E * ones(size(t)),': black')
        hold off;
%         ylim([0 2]);
        t1=legend('Willmore energy',W_E_text);
        set(t1,'Interpreter','latex','FontSize',13);
        t1=title('Willmore energy');
        set(t1,'Interpreter','latex','FontSize',16);
        t1=xlabel('time $t$');
        set(t1,'Interpreter','latex','FontSize',12);
        
        drawnow
%         waitforbuttonpress
case 5
        t1=sgtitle(title_text);
        set(t1,'Interpreter','latex','FontSize',16);
        
        subplot(1,2,1)
        plot(t,norms_nu(1,:),'*- black')
        hold on;
        plot(t,norms_nu(2,:),'o- black')
        hold off;
        ylim([0 2]);
%         ylim([0.8 1.2]);
        t1=legend('$\max\{|\nu_h|\}$','$\min\{|\nu_h|\}$');
        set(t1,'Interpreter','latex','FontSize',13);
        t1=title('length of $\nu_h$');
        set(t1,'Interpreter','latex','FontSize',16);
        t1=xlabel('time $t$');
        set(t1,'Interpreter','latex','FontSize',12);
        
        subplot(1,2,2)
        plot(t,Willmore_energy_vect,'s- red')
        hold on;
        plot(t,W_E * ones(size(t)),': black') 
        hold off;
%         ylim([0 2]);
        t1=legend('Willmore energy',W_E_text,'FontSize',13);
        set(t1,'Interpreter','latex','FontSize',13);
        t1=title('Willmore energy');
        set(t1,'Interpreter','latex','FontSize',16);
        t1=xlabel('time $t$');
        set(t1,'Interpreter','latex','FontSize',12);
        
        drawnow;
%         waitforbuttonpress
case 6
    
end