function dV = func_V_to_dV( V ,flow,alpha)

if strcmp(flow,'MCF')     % MCF: V(H) = H
    % inversion: H = V^{-1}(V)
    H = V;
    % evaluating V' = 1 at H
    dV = ones(size(H));
elseif strcmp(flow,'iMCF')     % inverse MCF: V(H) = - 1 / H
    % inversion: H = V^{-1}(V)
    H = -1 ./ V;
    % evaluating V' at H
    dV = 1 ./ ( H.^2 );
elseif strcmp(flow,'MCFgen')     % generalised MCF: V(H) = H^alpha 
    % inversion: H = V^{-1}(V)
    H = ( V ).^(1/alpha);
    % evaluating V' = alpha * H^(alpha-1)  at H
    dV = alpha .* ( H.^(alpha-1) );
elseif strcmp(flow,'iMCFgen')     % generalised inverse MCF: V(H) = - 1 / H^alpha 
    % inversion: H = V^{-1}(V)
    H = ( -1 ./ V ).^(1/alpha);
    % evaluating V' at H
    dV = alpha .* ( H.^(-alpha-1) );
else
    disp('Flow?!')
end
	

