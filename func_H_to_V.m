function V = func_H_to_V( H ,flow,alpha)

% computing V = V(H)

if strcmp(flow,'MCF')     % MCF: V(H) = H
    V = H;
elseif strcmp(flow,'iMCF')     % inverse MCF: V(H) = - 1 / H
    V = -1 ./ H;
elseif strcmp(flow,'MCFgen')     % generalised MCF: V(H) = H^alpha 
    V = H.^alpha;
elseif strcmp(flow,'iMCFgen')     % generalised inverse MCF: V(H) = - 1 / H^alpha 
    V = - 1 ./ (H.^alpha);
else
    disp('Flow?!')
end
	

