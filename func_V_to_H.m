function H = func_V_to_H( V ,flow,alpha)

% inversion: H = V^{-1}(V)

if strcmp(flow,'MCF')               % MCF: V(H) = H
    H = V;
elseif strcmp(flow,'iMCF')          % inverse MCF: V(H) = - 1 / H
    H = -1 ./ V;
elseif strcmp(flow,'MCFgen')        % generalised MCF: V(H) = H^alpha 
    H = ( V ).^(1/alpha);
elseif strcmp(flow,'iMCFgen')       % generalised inverse MCF: V(H) = - 1 / H^alpha 
    H = ( -1 ./ V ).^(1/alpha);
else
    disp('Flow?!')
end
	

