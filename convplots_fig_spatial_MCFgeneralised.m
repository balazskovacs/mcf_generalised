function convplots_fig_spatial_MCFgeneralised

% Creates spatial convergence plots (high order ESFEM; M and A norms).
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
format long;

k=2;                % BDF order

% v = - V(H) \nu, with V given as below
% flow = 'MCF';  flow_txt = ''; % MCF: V(H) = H
% flow = 'iMCF';  flow_txt = 'inverse '; % inverse MCF: V(H) = - 1 / H
% flow = 'MCFgen';  flow_txt = 'generalised '; % generalised MCF: V(H) = H^alpha 
flow = 'iMCFgen';  flow_txt = 'generalised inverse '; % generalised inverse MCF: V(H) = - 1 / H^alpha 

alpha = 2;          % flow parameter
if strcmp(flow,'MCF') || strcmp(flow,'iMCF')
    alpha = 1; % setting flow to 1 for mean curvature and inverse mean curvature flows
end


m=2;                % dimension
R0 = 3;
T = 1;              % final time   
T_text = '1';


% spatial refinements
n_vect=(0:4);
% temporal refinements
tau_vect=.2*2.^(-4:-1:-9);


poly_deg=2;

surface = 'Sphere';

if strcmp(surface,'Sphere')
    surface_txt = 'sphere';
    folder = 'MCFgen_test_Sphere'; save_txt = 'Sphere';
else
    disp('surface?')
end



% axis limit and reference line height
factor=10;
ylimits=[10^-5 10^1];
locationstring='SouthEast';
% locationstring='NorthWest';

%%
% figure

for j=1:length(tau_vect)
    tau=tau_vect(j);
    DOF{j}=strcat(['$\tau =',num2str(tau),'$']);
    for i=1:length(n_vect)
        n=n_vect(i);
        if exist(['results/',folder,'/errors_',flow,num2str(alpha),'_BDF',num2str(k),'_n',num2str(n),'_tau',num2str(tau),'.txt'], 'file')==2
            err=load(['results/',folder,'/errors_',flow,num2str(alpha),'_BDF',num2str(k),'_n',num2str(n),'_tau',num2str(tau),'.txt']);
            % L^\infty errors on [0,T]
            I=ismember(err(1,:),T);
            Points=(1:length(I));
            T_n = Points(I);
            error_x(j,i)=max(err(2,1:T_n));
            error_nu(j,i)=max(err(3,1:T_n));
            error_V(j,i)=max(err(4,1:T_n));
        else
            error_x(j,i)=NaN;
            error_nu(j,i)=NaN;
            error_V(j,i)=NaN;
        end
        
    end
end
for i=1:length(n_vect)
    n=n_vect(i);
    if exist(['../../surfs/',surface,'_meshdata',num2str(n),'.txt'], 'file')==2
        data=load(['../surfs/',surface,'_meshdata',num2str(n),'.txt']);
    else
        Nodes=load(['../surfs/',surface,'_nodes',num2str(n),'.txt']);
        Elements=load(['../surfs/',surface,'_elements',num2str(n),'.txt']);
        bars=[Elements(:,[1,2]);Elements(:,[1,3]);Elements(:,[2,3])];
        bars=unique(sort(bars,2),'rows');
        barvec=Nodes(bars(:,1),:)-Nodes(bars(:,2),:);
        h_vect=sqrt(sum(barvec.^2,2));
        data(1,1)=max(h_vect);
        data(2,1:length(h_vect.'))=h_vect.';
        save(['../surfs/',surface,'_meshdata',num2str(n),'.txt'], 'data', '-ASCII');
    end
    x(i)=data(1,1); % max
end

DOF{j+1} = '$\mathcal{O}(h^2)$';


%% plot
% f1=figure('position',[10 50 1066 400]);
f1=figure('position',[10 50 1200 500]);

t1=sgtitle([flow_txt,'mean curvature flow for a ',surface_txt,' in $[0,',num2str(T),']$']);
set(t1,'Interpreter','latex','FontSize',16);
        

subplot(1,3,1)
loglog_conv(x,error_x)
hold on; 
loglog(x,x.^(poly_deg)*factor,'-. black','LineWidth',1); 
hold off;
h1=title('$\|X-X_h\|_{L^\infty(H^1)}$');
set(h1,'Interpreter','latex','FontSize',16);
h1=xlabel('mesh size ($h$)');
set(h1,'Interpreter','latex','FontSize',16);
h1=ylabel('errors','Interpreter','latex');
set(h1,'Interpreter','latex','FontSize',16);
h2=legend(DOF,'Location',locationstring,'FontSize',10);
set(h2,'Interpreter','latex');
xlim([(1/1.2)*min(x) 1.2*max(x)])
ylim(ylimits)

subplot(1,3,2)
loglog_conv(x,error_nu)
hold on; 
loglog(x,x.^(poly_deg)*factor,'-. black','LineWidth',1);
hold off;
h1=title('$\|\nu-\nu_h\|_{L^\infty(H^1)}$');
set(h1,'Interpreter','latex','FontSize',16);
h1=xlabel('mesh size ($h$)');
set(h1,'Interpreter','latex','FontSize',16);
% ylabel('error','Interpreter','latex')
h2=legend(DOF,'Location',locationstring,'FontSize',10);
set(h2,'Interpreter','latex');
xlim([(1/1.2)*min(x) 1.2*max(x)])
ylim(ylimits)

subplot(1,3,3)
loglog_conv(x,error_V)
hold on; 
loglog(x,x.^(poly_deg)*factor,'-. black','LineWidth',1);
hold off;
h1=title('$\|H-H_h\|_{L^\infty(H^1)}$');
set(h1,'Interpreter','latex','FontSize',16);
h1=xlabel('mesh size ($h$)');
set(h1,'Interpreter','latex','FontSize',16);
% ylabel('error','Interpreter','latex')
h2=legend(DOF,'Location',locationstring,'FontSize',10);
set(h2,'Interpreter','latex');
xlim([(1/1.2)*min(x) 1.2*max(x)])
ylim(ylimits)


% h1=suptitle(['velocity law - ',velocity_law_text]);
% set(h1,'Interpreter','latex','FontSize',18);


saveas(f1,['figures/convplot_',flow,num2str(alpha),'_',save_txt,'_T',T_text,'_BDF',num2str(k),'_space_Linfty.fig']);
saveas(f1,['figures/convplot_',flow,num2str(alpha),'_',save_txt,'_T',T_text,'_BDF',num2str(k),'_space_Linfty.eps'], 'epsc2');


% % saving as a 'mat' file for to replotting
% s1.T=T;
% s1.n_vect=n_vect;
% s1.tau_vect=tau_vect;
% s1.DOF=DOF;
% s1.error_x=error_x;
% s1.error_nu=error_nu;
% s1.error_H=error_H;
% s1.Willmore_energy=Willmore_energy;
% save(['figures/convplot_Willmore_',surface_txt,'_T',T_text,'_space_Linfty.mat'],'-struct','s1');

end

function loglog_conv(vect,err)

[n1 n2]=size(err);

symbols='sox.d+^*v><';
ms=[6 6 6 8 6 6 6 6 6 6 6];
gr=(linspace(.66,0,n1))';
colors=[gr gr gr];

for jj=1:n1
    loglog(vect,err(jj,:), ...
           'LineWidth',1,...
           'Marker',symbols(jj),...
           'MarkerSize',ms(jj),...
           'Color', colors(jj,:));%'Color', 'black'); %
    if jj==1
        hold on;
    end
end
hold off;
end

function logx_W(vect,err)

[n1 n2]=size(err);

symbols='sox.d+^*v><';
ms=[6 6 6 8 6 6 6 6 6 6 6];
gr=(linspace(.66,0,n1))';
colors=[gr gr gr];

for jj=1:n1
    semilogx(vect,err(jj,:), ...
           'LineWidth',1,...
           'Marker',symbols(jj),...
           'MarkerSize',ms(jj),...
           'Color', colors(jj,:));%'Color', 'black'); %
    if jj==1
        hold on;
    end
end
hold off;
end