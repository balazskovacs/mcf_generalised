function R_out = func_radius_MCFgeneralised(t,R0 , flow,alpha)

% for a two diemansional sphere
m = 2;

if strcmp(flow,'MCF')
    % mean curvature flow 
    R_out = real( sqrt( R0^2 - 2*m*t ) );
elseif strcmp(flow,'iMCF')
    % inverse mean curvature flow 
    R_out = R0 * exp( t/m );
elseif strcmp(flow,'MCFgen')
    % generalised mean curvature flow 
    R_out = real( ( R0^(alpha+1) - (alpha+1) * m^alpha * t ) .^ (1/(alpha+1)) );
elseif strcmp(flow,'iMCFgen')
    % generalised inverse mean curvature flow 
    R_out = real( ( R0^(1-alpha) - (alpha-1) * m^(-alpha) * t ) .^ (1-alpha) );
end
