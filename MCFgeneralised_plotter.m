function MCFgeneralised_plotter(Nodes,Elements,Elements_plot,k,tau,T0,T, surf_name,u_init, plt,limits,view_angle,skipping,title_text,flow,alpha , varargin)

% This code plots solutions to generalised (inverse) mean curvature flows.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

format long;
    
%% initial surface

% degrees of freedom
dof=length(Nodes);

% saving initial mesh
Nodes0=Nodes;

%% structure of variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% surface:
% --> the tensor x (dimension: dof \times 3 \times T/tau) stores 
%     the surface nodes at time t_k in its k-th layer x(:,:,k)
% --> the matrix x(:,:,k) (dimension: dof \times 3) stores the nodes of the
%     discrete surface (coordinates are column-wise, nodes row-wise)
% 
% dynamic variables:
% --> the tensor u (dimension: dof \times 4 \times T/tau) stores
%     the nodeal values of the dynamic variables (n_h,H_h) at time t_k
%     in its k-th layer u(:,:,k)
% --> the matrix u(:,:,k) (dimension: dof \times 4) stores 
%     the nodal values of n_h in its first three columns, and
%     the nodal values of H_h in its fourth column
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% video
if plt==2
    n_now=now; % to prevent overwriting existing files
    vidObj = VideoWriter(['vids/',flow,num2str(alpha),'_',surf_name,'_',num2str(n_now)]);
    vidObj.Quality = 100;
    vidObj.FrameRate = 24;
    open(vidObj);
end


%% initial values
% initialise time
t=T0;
t_new = T0;

% positions
x_new(:,:,1)=Nodes;
u_new = u_init;

if plt == 1 || plt == 2
    fig=figure('Position', [10, 50, 1280, 720]);
    plotter_script;
%     waitforbuttonpress;
end
if plt==2
    saveas(fig,['vids/',flow,num2str(alpha),'_',surf_name,'_',num2str(n_now),'.png']);
    close(fig);
end


            
if plt == 6
    f1 = figure;

    trisurf(Elements_plot,x_new(:,1),x_new(:,2),x_new(:,3),0)
    axis('equal');
    t1=zlabel(['time $t=',num2str(t_new),'$']);
    set(t1,'Interpreter','latex','FontSize',18,'HorizontalAlignment', 'left');
%     view([1 0 0]);
    drawnow
    

    saveas(f1,['figures_monotone/',flow,num2str(alpha),'_surf_',surf_name,'_',num2str(t_new),'.fig']);
    saveas(f1,['figures_monotone/',flow,num2str(alpha),'_surf_',surf_name,'_',num2str(t_new),'.eps'], 'epsc2');

    pause(0.42)
    close(f1);
end
            
% waitforbuttonpress


%% time stepping
for steps=k:ceil((T-T0)/tau)
    % timestep
    t_new=steps*tau;
    
    % loading data
    if mod(steps,skipping)==0 % basic
%     if min( abs( (0:0.0025:T) - t_new ) ) < tau/2 % very fine time stepping
        if exist(['results/MCFgen_test_',surf_name,'/',flow,num2str(alpha),'_solution_at_t',num2str(t_new),'_BDF',num2str(k),varargin{:},'_tau',num2str(tau),'.mat'])
            s1=load(['results/MCFgen_test_',surf_name,'/',flow,num2str(alpha),'_solution_at_t',num2str(t_new),'_BDF',num2str(k),varargin{:},'_tau',num2str(tau),'.mat']);
        elseif exist(['results/MCFgen_test_',surf_name,'/',flow,num2str(alpha),'_solution_at_t',num2str(ceil(t_new*10^6)*10^(-6)),'_BDF',num2str(k),varargin{:},'_tau',num2str(tau),'.mat'])
            s1=load(['results/MCFgen_test_',surf_name,'/',flow,num2str(alpha),'_solution_at_t',num2str(ceil(t_new*10^6)*10^(-6)),'_BDF',num2str(k),varargin{:},'_tau',num2str(tau),'.mat']);
        elseif exist(['results/MCFgen_test_',surf_name,'/',flow,num2str(alpha),'_solution_at_t',num2str(floor(t_new*10^6)*10^(-6)),'_BDF',num2str(k),varargin{:},'_tau',num2str(tau),'.mat'])
            s1=load(['results/MCFgen_test_',surf_name,'/',flow,num2str(alpha),'_solution_at_t',num2str(floor(t_new*10^6)*10^(-6)),'_BDF',num2str(k),varargin{:},'_tau',num2str(tau),'.mat']);
        elseif exist(['results/MCFgen_test_',surf_name,'/',flow,num2str(alpha),'_solution_at_t',num2str(ceil(t_new*10^7)*10^(-7)),'_BDF',num2str(k),varargin{:},'_tau',num2str(tau),'.mat'])
            s1=load(['results/MCFgen_test_',surf_name,'/',flow,num2str(alpha),'_solution_at_t',num2str(ceil(t_new*10^7)*10^(-7)),'_BDF',num2str(k),varargin{:},'_tau',num2str(tau),'.mat']);
        elseif exist(['results/MCFgen_test_',surf_name,'/',flow,num2str(alpha),'_solution_at_t',num2str(floor(t_new*10^7)*10^(-7)),'_BDF',num2str(k),varargin{:},'_tau',num2str(tau),'.mat'])
            s1=load(['results/MCFgen_test_',surf_name,'/',flow,num2str(alpha),'_solution_at_t',num2str(floor(t_new*10^7)*10^(-7)),'_BDF',num2str(k),varargin{:},'_tau',num2str(tau),'.mat']);
        end
        x_new=s1.x_new;
        u_new=s1.u_new;
        % plotting
        if plt == 1 
            plotter_script;
%             disp('')
%             waitforbuttonpress
        elseif plt == 2
            fig=figure('Position', [10, 50, 1280, 720]);
            plotter_script;
            close(fig); 
        end
        
    end 
    
    if plt == 6
%         if min( abs([0.1 0.2 0.5 1] - t_new)) < tau/4 
%         if min( abs([0.00625 0.0125 0.025 0.0375 0.05 0.0625 0.065625] - t_new)) < tau/4 
        if min( abs([0.2 0.4] - t_new)) < tau/4 %  0.5 0.6 0.7
%         if min( abs([3] - t_new)) < tau/4 
            f1 = figure;
            
            trisurf(Elements_plot,x_new(:,1),x_new(:,2),x_new(:,3),0)
            axis('equal');
            t1=zlabel(['time $t=',num2str(t_new),'$']);
            set(t1,'Interpreter','latex','FontSize',18,'HorizontalAlignment', 'left');
%             view([1 0 0]);
            drawnow
            
            saveas(f1,['figures_monotone/',flow,num2str(alpha),'_surf_',surf_name,'_',num2str(t_new),'.fig']);
            saveas(f1,['figures_monotone/',flow,num2str(alpha),'_surf_',surf_name,'_',num2str(t_new),'.eps'], 'epsc2');
            
            pause(0.42)
            close(f1);
        end
    end
    
    % new time
    t=[t t_new];
end
% disp('')
if plt==2
    close(vidObj);
end

