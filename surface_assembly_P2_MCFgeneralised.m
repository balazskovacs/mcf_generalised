function [M,A,Mxu,f,g1,g2] = surface_assembly_P2_MCFgeneralised(Nodes,Elements, u , flow,alpha)
% A combined surface assembly for surface finite elements 
% for generalised mean curvature flow algorithm.
% 'flow' determines the function V, while 'alpha' is a parameter therein.
% 
% 
% - two dimensional surfaces in three dimensions
% - quadratic isoparametric finite elements
% 
% Written in P2Q2Iso2D approach.
% Using cell structure conversion for sparse matrices;
% see: https://de.mathworks.com/matlabcentral/answers/203734-most-efficient-way-to-add-multiple-sparse-matrices-in-a-loop-in-matlab
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

format long;

% the quadrature nodes and the number of quadrature nodes
W = [0.072157803838894   0.047545817133642   0.047545817133642   0.047545817133642   0.051608685267359   0.051608685267359   0.051608685267359 0.016229248811599   0.016229248811599   0.016229248811599   0.013615157087217   0.013615157087217   0.013615157087217   0.013615157087217 0.013615157087217   0.013615157087217];
length_W = length(W);
                   
%% load or compute and save the (precomputed) product tensors
if exist(['assembly_precomp_P2/precomp_values_P2_',num2str(length_W),'.mat'], 'file')==2
    precomp_values=load(['assembly_precomp_P2/precomp_values_P2_',num2str(length_W),'.mat']);
    F_eval = precomp_values.F_eval;
    M_eval = precomp_values.M_eval;
    grad_F_eval = precomp_values.grad_F_eval;
else
    disp(['Computing and storing precomputeable values (M=',num2str(length_W),').'])
    % Dunavant quadrature of order 5 nodes and weights
    xieta = [0.333333333333333   0.081414823414554   0.459292588292723   0.459292588292723   0.658861384496480   0.170569307751760   0.170569307751760   0.898905543365938   0.050547228317031   0.050547228317031   0.008394777409958   0.263112829634638   0.728492392955404   0.263112829634638   0.728492392955404   0.008394777409958;
             0.333333333333333   0.459292588292723   0.459292588292723   0.081414823414554   0.170569307751760   0.170569307751760   0.658861384496480   0.050547228317031   0.050547228317031   0.898905543365938   0.263112829634638   0.728492392955404   0.008394777409958   0.008394777409958   0.263112829634638   0.728492392955404;
                             0                   0                   0                   0                   0                   0                   0                   0                   0                   0                   0                   0                   0                   0                   0                   0;];
    % precomputing data                     
    [F_eval,M_eval, grad_F_eval] = surface_assembly_precompute_for_P2(xieta,W);
    % saving data in the right directory, and creating directory if it not exists
    if exist('assembly_precomp_P2', 'dir') ~= 7
        mkdir('assembly_precomp_P2');
    end
    save(['assembly_precomp_P2/precomp_values_P2_',num2str(length_W),'.mat'],'F_eval','M_eval','grad_F_eval');
end

%% inicialization
% local gegrees of freedom, and full one vector
dof_loc = 6; % due to quadratic elements
e_loc_dof = ones(1,dof_loc);
% degree of freedom
dof = length(Nodes);

%% modifications of the loaded data
% !!! should be rewritten to best fit the code --> 
pa_xi_F_eval(1:6,1:length_W)=grad_F_eval(1,:,:);
pa_eta_F_eval(1:6,1:length_W)=grad_F_eval(2,:,:);
% <-- should be rewritten to best fit the code !!!

%% preallocation of cells end arrays
M_Cell = cell(size(Elements,1),1);
A_Cell = cell(size(Elements,1),1);

Mxu_Cell = cell(size(Elements,1),1);

f_Cell = cell(size(Elements,1),1);

g1_Cell = cell(size(Elements,1),1);
g2_Cell = cell(size(Elements,1),1);

% array_columns_1 = reshape(e_loc_dof.',[6 1]);
array_columns_3 = reshape([1 2 3] .* e_loc_dof.',[18 1]);
array_columns_4 = reshape([1 2 3 4] .* e_loc_dof.',[24 1]);

%% assembly
for i=1:size(Elements,1) % begin loop over elements
    %% element related objects
    % element arrays for matrices
    Element_array_rows = reshape(e_loc_dof .* Elements(i,:).',[36 1]);
    Element_array_columns = reshape(Elements(i,:) .* e_loc_dof.',[36 1]);
    % Note: for symmetric matrices the do not matter!
    
    % element row arrays for vectors (dimension: dof x 3 and dof x 4)
%     array_rows_1 = reshape(Elements(i,:).',[6 1]);
    array_rows_3 = reshape(ones(1,3) .* Elements(i,:).',[18 1]);
    array_rows_4 = reshape(ones(1,4) .* Elements(i,:).',[24 1]);
    
    % vertices of current element (as a 6 x 3 matrix)
    nodes_of_element = Nodes(Elements(i,:),:);
    
    % local nodal values of the input vectors u and w
    u_loc = u(Elements(i,:),:);
    
    %% element transofrmation and evaluations
    % the partial derivatives of the element transofrmation map
    pa_xi_Phi = nodes_of_element.' * pa_xi_F_eval;
    pa_eta_Phi = nodes_of_element.' * pa_eta_F_eval;
    % the third column of the Jacobian of the element transformation map
    % (the cross product of the two partial derivatives)
    D_3 = cross(pa_xi_Phi,pa_eta_Phi);
    % length (Euclidean norm) of the cross product
    nrm_cross = vecnorm(D_3,2,1);
    
    % the Jacobian of the element transformation map
    D(1:3,1,:) = pa_xi_Phi;
    D(1:3,2,:) = pa_eta_Phi;
    D(1:3,3,:) = D_3;
    
    % the pointwise product of the quadrature weights and the norm of the cross product
    prod_W_nrm = (W .* nrm_cross).';
    
    %% mass matrix assembly
    % M|_{kj} = \int_{\Ga_h} \phi_j \phi_k
   
    % the vectorized local matrix (the sum for the quadrature)
    MLoc_vec = reshape(M_eval,[36 length_W]) * prod_W_nrm;
    
    M_Cell{i} = [Element_array_rows Element_array_columns MLoc_vec];
    
    %% geometric quantities (A_h and alpha_h, and H_h hence dV_h)

%     V_h = F_eval * u_loc(:,4);
    dV_h = func_V_to_dV( F_eval * u_loc(:,4) , flow,alpha);

    if ~isreal(dV_h)
        V_h = F_eval * u_loc(:,4);
        V_h( V_h < 0) = min( abs( V_h ) );
        dV_h = func_V_to_dV( V_h , flow,alpha);
%         disp('')
    end
    
    % A_h terms (involving gradient of nu_h)
    % inverting the Jacobians, computing gradients of basis functions, and performing further calculations
    for ell = 1:length_W
        % computations for the stiffness matrix A
        C_ell = inv(D(:,:,ell)); 
        B_ell = C_ell.' * grad_F_eval(:,:,ell);
%         Bt(:,:,ell) = B_ell.';                  % preallocation slows
        BtB(:,:,ell) = B_ell.' * B_ell;         % preallocation slows
        
        % computations for multiple terms
        % alpha_h := |A_h|
        A_ell = 0.5 * ( B_ell * u_loc(:,1:3) + (B_ell * u_loc(:,1:3)).');
        alpha_vec(ell,1) = norm( A_ell  ,'fro');
    end
    
    %% stiffness matrix assembly
    % A|_{kj} = \int_{\Ga_h} \nb_{\Ga_h} \phi_j \cdot \nb_{\Ga_h} \phi_k
    
    % the vectorized local matrix (the sum for the quadrature)
    ALoc_vec = reshape(BtB,[36 length_W]) * prod_W_nrm;
    
    A_Cell{i} = [Element_array_rows Element_array_columns ALoc_vec];
    
    %% M(x,u) matrix for Willmore flow
    % Mxu|_{kj} =  \int_{\Ga_h} (\widetilde V_h')^(-1) \phi_j \phi_k
   
    prod_W_nrm_dV = prod_W_nrm .* ( 1 ./ dV_h );
    
    % the vectorized local matrix (the sum for the quadrature)
    MxuLoc_vec = reshape(M_eval,[36 length_W]) * prod_W_nrm_dV;
    
    Mxu_Cell{i} = [Element_array_rows Element_array_columns MxuLoc_vec];
    
    %% semi-linear term in evolution equations of MCF
    % f|_{k} = \int_{\Ga_h} |\nb_{\Ga_h}\nu_h|^2 u_h \phi_k 
    % for u_h = (\nu_h)_ell (ell = 1,2,3) and u_h = V_h 
    
    % product of the quadrature weights, cross product norm, the alpha values and the values of the function u_h 
    prod_W_nrm_u_loc = (prod_W_nrm .* (alpha_vec.^2)) .* (F_eval * u_loc);
    
    % the vectorised version of fLoc
    fLoc_vect = reshape((prod_W_nrm_u_loc.' * F_eval).',[24 1]);
    
    
    f_Cell{i} = [array_rows_4 array_columns_4 fLoc_vect];
    
    %% non-linear term in velocity law of MCF
    % g1|_{k} = \int_{\Ga_h} u_h w_h \phi_k 
    % for u_h = (\nu_h)_ell (ell = 1,2,3) and w_h = V_h 
    
    prod_W_nrm_u_loc = prod_W_nrm .* ( F_eval * u_loc(:,1:3) ) .* ( F_eval * u_loc(:,4) );
    
    % the vectorised version of fLoc
    g1Loc_vect = reshape((prod_W_nrm_u_loc.' * F_eval).',[18 1]);
        
    g1_Cell{i} = [array_rows_3 array_columns_3 g1Loc_vect];
    
    %% non-linear terms in velocity law of MCF
    % g2_l|_k = \int_{\Ga_h} \nbgh (V_h (\nu_h)_l) \cdot \nbgh \phi_k  for l = 1,2,3
    % in fact, computing the equivalent expression:
    % g2_l|_k =  \int_{\Ga_h} V_h \nbgh (\nu_h)_l \cdot \nbgh \phi_k (=:g2_1)
    %          + \int_{\Ga_h} (\nu_h)_l \nbgh V_h \cdot \nbgh \phi_k (=:g2_2)
%     ---
    
    prod_W_nrm_H_loc = prod_W_nrm .* ( F_eval * u_loc(:,4) );
    for ell = 1:length_W
        prod_BtB_nu_loc(:,:,ell) = BtB(:,:,ell) * u_loc(:,1:3);
    end
    
    % the vectorized local vector (the sum for the quadrature)
%     g2Loc_vect_1 = reshape(prod_BtB_nu_loc,[18 length_W]) * prod_W_nrm_H_loc;
    
    prod_W_nrm_nu_loc = prod_W_nrm .* ( F_eval * u_loc(:,1:3) );
    for ell = 1:length_W
        prod_BtB_H_loc(:,:,ell) = BtB(:,:,ell) * u_loc(:,4);
    end
    
    % the vectorized local vector (the sum for the quadrature)
    g2Loc_vect = reshape(prod_BtB_nu_loc,[18 length_W]) * prod_W_nrm_H_loc ...
                + reshape ( reshape(prod_BtB_H_loc,[6 length_W]) * prod_W_nrm_nu_loc , [18 1]);
    
    g2_Cell{i} = [array_rows_3 array_columns_3 g2Loc_vect];

end % end loop over elements

%% the actual matrix assmeblies
% IMPORTANT: 'sparse' sums for repeated indeces!
M_Cell_to_sparse = cell2mat( M_Cell );
M = sparse(M_Cell_to_sparse(:,1),M_Cell_to_sparse(:,2),M_Cell_to_sparse(:,3),dof,dof);
A_Cell_to_sparse = cell2mat( A_Cell );
A = sparse(A_Cell_to_sparse(:,1),A_Cell_to_sparse(:,2),A_Cell_to_sparse(:,3),dof,dof);

Mxu_Cell_to_sparse = cell2mat( Mxu_Cell );
Mxu = sparse(Mxu_Cell_to_sparse(:,1),Mxu_Cell_to_sparse(:,2),Mxu_Cell_to_sparse(:,3),dof,dof);

f_Cell_to_sparse = cell2mat( f_Cell );
f = sparse(f_Cell_to_sparse(:,1),f_Cell_to_sparse(:,2),f_Cell_to_sparse(:,3),dof,4);

g1_Cell_to_sparse = cell2mat( g1_Cell );
g1 = sparse(g1_Cell_to_sparse(:,1),g1_Cell_to_sparse(:,2),g1_Cell_to_sparse(:,3),dof,3);

g2_Cell_to_sparse = cell2mat( g2_Cell );
g2 = sparse(g2_Cell_to_sparse(:,1),g2_Cell_to_sparse(:,2),g2_Cell_to_sparse(:,3),dof,3);

end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [F_eval,M_eval, grad_F_eval]=surface_assembly_precompute_for_P2(xieta,W)
% This code precomputes and saves various evaluations of reference basis functions,
% and products of such evaluations in the quadrature nodes.
% 
% - standard reference element (with nodes (0,0), (1,0), (0,1), (0.5,0), (0.5,0.5), (0,0.5))
% - standard quadratic finite elements
% 
% Written for P2Q2 approach.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% basis functions (or form functions) on the reference element
% Anonymous functions are used, for easy modification for other element types.
F=@(xi,eta)[(2*xi+2*eta-1).*(xi+eta-1) xi.*(2*xi-1) eta.*(2*eta-1)  -4*xi.*(xi+eta-1) 4*xi.*eta -4*eta.*(xi+eta-1)];

% evaluating the basis functions on the reference element
F_eval=[];
for ind=1:length(W)
    F_eval=[F_eval ; F(xieta(1,ind),xieta(2,ind))];
end

% Mass matrix assembly: This code precomputes and saves 
% the 6 x 6 x M tensor containing the products of the
% basis functions on the reference element:
%   M_eval|_{kjl} = \vphi_k(xi_l) \vphi_j(xi_l)
M_eval=zeros(6,6,length(W));
for ell=1:length(W) 
    F_ell=F_eval(ell,:);
    M_eval(:,:,ell)=F_ell.'*F_ell;
end

% Stiffness matrix assembly: 
% Evaluating gradients of the basis functions in the quadrature nodes.
% grad F1
grad_F1=@(xi,eta) [4*xi + 4*eta - 3;4*xi + 4*eta - 3;0];
% grad F2
grad_F2=@(xi,eta) [4*xi - 1;0;0];
% grad F3
grad_F3=@(xi,eta) [0;4*eta - 1;0];

% grad F4
grad_F4=@(xi,eta) [-4*(2*xi + eta - 1);-4*xi;0];
% grad F5
grad_F5=@(xi,eta) [4*eta;4*xi;0];
% grad F6
grad_F6=@(xi,eta) [-4*eta;-4*(xi + 2*eta - 1);0];

for ind=1:length(W)
    grad_F_eval(:,:,ind)=[grad_F1(xieta(1,ind),xieta(2,ind)) grad_F2(xieta(1,ind),xieta(2,ind)) grad_F3(xieta(1,ind),xieta(2,ind))  grad_F4(xieta(1,ind),xieta(2,ind)) grad_F5(xieta(1,ind),xieta(2,ind)) grad_F6(xieta(1,ind),xieta(2,ind))];
end
end