function MCFgeneralised_runscript_special
% 
% Runscript for generalised (inverse) mean curvature flow for any surface.
%
% Required inputs: 
%   - quadratic initial triangulation
%   - initial values: normal and mean curvature
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
format long;

k=2;                % BDF order

% v = - V(H) \nu, with V given as below
% flow = 'MCF';         % MCF: V(H) = H
% flow = 'iMCF';        % inverse MCF: V(H) = - 1 / H
flow = 'MCFgen';      % generalised MCF: V(H) = H^alpha 
% flow = 'iMCFgen';     % generalised inverse MCF: V(H) = - 1 / H^alpha 

alpha = 6;            % flow parameter

if strcmp(flow,'MCF') || strcmp(flow,'iMCF')
    alpha = 1; % setting flow to 1 for mean curvature and inverse mean curvature flows
end

T = 2;              % final time
m = 2;              % surface dimension

% time step size
tau=.2*2.^(-4)


%% loading mesh information
%%%%%%%%%% 
% % Sphere
% surf_name = 'Sphere';
% % distance function 
% fd=@(x) x(:,1).^2 + x(:,2).^2 + x(:,3).^2 - 1;
% grad_fd = @(x) [2 * x(:,1) ...
%                 2 * x(:,2) ...
%                 2 * x(:,3)];

%%%%%%%%%% 
% % Ellipsoid
% surf_name = 'Ellipsoid';
% % distance function 
% fd=@(x) x(:,1).^2/2^2 + x(:,2).^2/2^2 + x(:,3).^2 - 1;
% grad_fd = @(x) [2 * x(:,1)/2^2 ...
%                 2 * x(:,2)/2^2 ...
%                 2*x(:,3)];

%%%%%%%%%% 
% Ellipsoid3 or Ellipsoid3_L (large!)
% surf_name = 'Ellipsoid3';
% % distance function 
% fd=@(x) x(:,1).^2/3^2 + x(:,2).^2/2^2 + x(:,3).^2/1^2 - 1;
% 
% % surf_name = 'Ellipsoid3_L';
% % % distance function 
% % fd=@(x) x(:,1).^2/3^2 + x(:,2).^2/2^2 + x(:,3).^2/1^2 - 2^2;
% 
% grad_fd = @(x) [2 * x(:,1)/3^2 ...
%                 2 * x(:,2)/2^2 ...
%                 2 * x(:,3)/1^2];

%%%%%%%%%% 
% % Cuboid
surf_name = 'Cuboid';
% distance function 
fd = @(x) abs(x(:,1)).^2.5 + abs(x(:,2)).^2.5 + abs(x(:,3)).^2.5 - (2.335)^2.5;
grad_fd = 'too_complicated';

%%%%%%%%%% 
% % Dumbbell (Elliott-Styles surface)
% surf_name = 'Dumbbell';
% % distance function 
% fd=@(x) x(:,1).^2 + x(:,2).^2 + 2*(x(:,3).^2).*(x(:,3).^2-199/200) - 0.01;
% grad_fd = 'too_complicated';


%%%%%%%%%% 
% % Dumbbell for inverse MCF
% surf_name = 'Dumbbell_iMCF';
% % distance function 
% fd=@(x) x(:,1).^2 + x(:,2).^2 + 2*(x(:,3).^2).*(x(:,3).^2-179/200) - 0.1;
% grad_fd = 'too_complicated';

%%%%%%%%%% 
% % a "fat" Dumbbell for inverse MCF
% % surf_name = 'Dumbbell2_iMCF';
% surf_name = 'Dumbbell2_MCFgen';
% % surf_name = 'Dumbbell2_iMCFgen';
% % distance function 
% fd=@(x) x(:,1).^2 + x(:,2).^2 + 2*(x(:,3).^2).*(x(:,3).^2-159/200) - 0.5;
% grad_fd = 'too_complicated';

%%%%%%%%%% 
% % GenusBall
% surf_name = 'GenusBall2';
% % distance function 
% fd=@(x) (x(:,1).^2-1).^2 + (x(:,2).^2-1).^2 + (x(:,3).^2-1).^2 - 1.05;
% grad_fd = @(x) [2 .* (x(:,1).^2-1) .* 2.*x(:,1) ...
%          2 .* (x(:,2).^2-1) .* 2.*x(:,2) ...
%          2 .* (x(:,3).^2-1) .* 2.*x(:,3)];


%% loading P2 mesh
addpath('../aux_func');
[Nodes,Elements,Elements_plot] = load_mesh_p2(surf_name,fd);

dof=length(Nodes)

%% initial data
% normal vector
Normal = load_normal_p2(surf_name,Nodes,Elements_plot,fd,grad_fd);
length_Normal = sqrt(sum(Normal.^2, 2));

% mean curvature
[M,H] = surface_assembly_P2_H(Nodes,Elements, Normal);

% H(abs(H) < 0.05) = 0.05;

u_init = [Normal func_H_to_V( H ,flow,alpha)];


rmpath('../aux_func');

%% plot initial surface and initial data
% figure
% % trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), fd(Nodes) ) %
% trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), u_init(:,4) ) %
% % trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), H ) %
% colorbar
% hold on;
% quiver3(Nodes(:,1),Nodes(:,2),Nodes(:,3),u_init(:,1),u_init(:,2),u_init(:,3))
% hold off;
% axis('equal')
% title('mesh, normal vector and mean curvature')
% 
% drawnow
% disp('')

%% calling algorithm

% MCFgeneralised_solver(Nodes,Elements,Elements_plot,u_init,k,tau,m,T, flow,alpha, ['_',surf_name]);

%%%%%%%%%%%%
% plotting
%%%%%%%%%%%%
% fig=figure('Position', [10, 50, 1280, 720]);
% plt=0; % no plotting case, due to runscripts
% plt=1; % only plotting
plt=2; % video
% plt=6; % frames


limits = 2.35 * [-1 1; -1 1; -1 1]; % spherical
% limits = [-1.35 1.35; -1.35 1.35; -1.75 1.75]; % fat dumbbell iMCF
% limits = [-1 1; -1 1; -1.15 1.15]; % fat dumbbell MCFgen2
view_angle = [1 0.82 0.42];
% view_angle = [1 1 0];
% view_angle = [1 0 0];
skipping = 2;
T0 = 0;     % starting time

% title_text = 'generalised MCF for a cuboid';
% title_text = 'inverse mean curvature flow of a dumbbell';
title_text = 'generalised mean curvature flow of cuboid';
% title_text = 'generalised inverse mean curvature flow of a dumbbell';
% title_text = 'generalised inverse mean curvature flow of a genus-5 surface';

MCFgeneralised_plotter(Nodes,Elements,Elements_plot,k,tau,T0,T, surf_name,u_init, plt,limits,view_angle,skipping,title_text,flow,alpha);

%% Hawking mass for inverse MCF
% if strcmp(flow,'iMCF')          % inverse MCF: V(H) = - 1 / H
%     title_text = 'Hawking mass for inverese MCF of an ellipsoid';
%     T0 = 0;
%     Hawkingmass_plotter(Nodes,Elements,Elements_plot,k,tau,T0,T, surf_name,title_text, flow,alpha);
% elseif strcmp(flow,'MCFgen')          % inverse MCF: V(H) = - 1 / H
%     title_text = '$m_{\textnormal{$H^\alpha$-flow}}$ for generalised MCF of an ellipsoid';
%     T0 = 0; 
%     m_Halpha_plotter(Nodes,Elements,Elements_plot,k,tau,T0,T, surf_name,title_text, flow,alpha);
% end

beep;

end