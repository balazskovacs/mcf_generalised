function MCFgeneralised_solver(Nodes,Elements,Elements_plot,u_init,k,tau,m,T, flow,alpha, varargin)
% 
% Generalised (inverse) mean curvature flow for any initial surface.
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
format long;

%% initial surface

% degrees of freedom
dof = length(Nodes);
% constant 1 vector
one_vec = ones(dof,1);

% saving initial mesh
Nodes0 = Nodes;

%% structure of variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% surface:
% --> the tensor x (dimension: dof \times 3 \times T/tau) stores 
%     the surface nodes at time t_k in its k-th layer x(:,:,k)
% --> the matrix x(:,:,k) (dimension: dof \times 3) stores the nodes of the
%     discrete surface (coordinates are column-wise, nodes row-wise)
% 
% dynamic variables:
% --> the tensor u (dimension: dof \times 4 \times T/tau) stores
%     the nodeal values of the dynamic variables (n_h,H_h) at time t_k
%     in its k-th layer u(:,:,k)
% --> the matrix u(:,:,k) (dimension: dof \times 4) stores 
%     the nodal values of n_h in its first three columns, and
%     the nodal values of H_h in its fourth column
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% initial values
t = [];
t_new = 0;
t = [t t_new];
x_past(:,:,1) = Nodes0;
u_past(:,:,1) = u_init;

% Hawking mass
if strcmp(flow,'iMCF')
    [M,~,~,~,~,~] = surface_assembly_P2_MCFgeneralised(x_past(:,:,1),Elements, u_past(:,:,1) , flow,alpha);
    H_new = func_V_to_H( u_init(:,4) ,flow,alpha);
    Hawking_mass = ( (one_vec.' * M * one_vec) / (16*pi) ).^(0.5) * ( 1 - (H_new.' * M * H_new) / (16*pi) );
elseif strcmp(flow,'MCFgen')
    [M,M_m_Halpha] = surface_assembly_P2_Halpha_monotone(x_past(:,:,1),Elements, u_past(:,:,1) , flow,alpha);
    m_Halpha = max( M \ M_m_Halpha );
end

for k_ind = 1:k-1
    
    % BDF coefficients
    [delta,gamma] = BDF_tableau(k_ind);
    
    t_new = k_ind*tau;
    
    % extrapolations
    x_tilde=zeros(dof,3);
    u_tilde=zeros(dof,4);
    for ind=1:k_ind
        % surface
        x_tilde=x_tilde+gamma(k_ind-ind+1)*x_past(:,:,end-k_ind+ind);
        % dynamic variables u = (nu,V)
        u_tilde=u_tilde+gamma(k_ind-ind+1)*u_past(:,:,end-k_ind+ind);
    end
    
    %%%%%%%%
    % projecting nu_h onto the unit sphere
    u_tilde(:,1:3) = u_tilde(:,1:3) ./ sqrt( sum(u_tilde(:,1:3).^2,2) );
    %%%%%%%%
    
    %%%%%%%%
    % surface FEM assembly on the EXTRAPOLATED surface
    [M,A,Mxu,f,g1,g2] = surface_assembly_P2_MCFgeneralised(x_tilde,Elements, u_tilde , flow,alpha);
    
    % the K matrix from the H^1 inner product
    K = M + A;
    % g -- r.h.s. of the velocity law
    g = - g1 - g2;
     
    % contributions from the past
    % rho_v
    dx=0*x_tilde;
    du=0*u_tilde;
    for ind = 1:k_ind
        dx = dx + delta(ind+1) * x_past(:,:,end-ind+1);
        du = du + delta(ind+1) * u_past(:,:,end-ind+1);
    end
    rho_v = tau * g - K * dx;
    rho_u = tau * f - Mxu * du;
    
    % solving the 3 + 4 decoupled linear systems
    % velocity law
    x_new = (delta(1)*K) \ rho_v;
    % dynamic variables
    u_new = ( delta(1)*Mxu + tau*A ) \ rho_u;
    %%%%%%%%
    
    %%%%%%%%
    % projecting nu_h onto the unit sphere
    u_new(:,1:3) = u_new(:,1:3) ./ sqrt( sum(u_new(:,1:3).^2,2) );
    %%%%%%%%
    
    %%%%%%%%
    % updating the solutions and the time vector
    % new surface
    x_past(:,:,k_ind+1)=x_new;
    % new dynamic variables
    u_past(:,:,k_ind+1)=u_new;
    
    % new time
    t=[t t_new];
    
    % Hawking mass
    if strcmp(flow,'iMCF')
        H_new = func_V_to_H( u_new(:,4) ,flow,alpha);
        Hawking_mass = [Hawking_mass ( (one_vec.' * M * one_vec) / (16*pi) ).^(0.5) * ( 1 - (H_new.' * M * H_new) / (16*pi) )];
    elseif strcmp(flow,'MCFgen')
        [M,M_m_Halpha] = surface_assembly_P2_Halpha_monotone(x_new,Elements, u_new , flow,alpha);
        m_Halpha = [m_Halpha max( M \ M_m_Halpha )];
    end
    
    % plotting on the fly
%     plt=1;
%     limits=4*[-1 1; -1 1; -1 1]; %x-y-z limits
%     view_angle = [1 1 1];
%     skipping = 1;
%     title_text = [flow,' $\alpha = ',num2str(alpha),'$'];
%     plotter_script;
% %     pause
end


%% BDF coefficients
[delta,gamma] = BDF_tableau(k);

%% time Integration
for steps = k:ceil(T/tau)
    % timestep
    t_new = t(end) + tau
    
    % extrapolations
    x_tilde=zeros(dof,3);
    u_tilde=zeros(dof,4);
    for ind=1:k
        % surface
        x_tilde=x_tilde+gamma(k-ind+1)*x_past(:,:,end-k+ind);
        % dynamic variables u = (nu,V)
        u_tilde=u_tilde+gamma(k-ind+1)*u_past(:,:,end-k+ind);
    end
    
    %%%%%%%%
    % projecting nu_h onto the unit sphere
    u_tilde(:,1:3) = u_tilde(:,1:3) ./ sqrt( sum(u_tilde(:,1:3).^2,2) );
    %%%%%%%%
    
    %%%%%%%%
    % surface FEM assembly on the EXTRAPOLATED surface
    [M,A,Mxu,f,g1,g2] = surface_assembly_P2_MCFgeneralised(x_tilde,Elements, u_tilde , flow,alpha);
    
    % the K matrix from the H^1 inner product
    K = M + A;
    % g -- r.h.s. of the velocity law
    g = - g1 - g2;
     
    % contributions from the past
    % rho_v
    dx=0*x_tilde;
    du=0*u_tilde;
    for ind = 1:k
        dx = dx + delta(ind+1) * x_past(:,:,end-ind+1);
        du = du + delta(ind+1) * u_past(:,:,end-ind+1);
    end
    rho_v = tau * g - K * dx;
    rho_u = tau * f - Mxu * du;
    
    % solving the 3 + 4 decoupled linear systems
    % velocity law
    x_new = (delta(1)*K) \ rho_v;
    % dynamic variables
    u_new = ( delta(1)*Mxu + tau*A ) \ rho_u;
    %%%%%%%%
    
    %%%%%%%%
    % projecting nu_h onto the unit sphere
    u_new(:,1:3) = u_new(:,1:3) ./ sqrt( sum(u_new(:,1:3).^2,2) );
    %%%%%%%%
    
    %%%%%%%%
    % updating the solutions and the time vector
    % new surface
    x_past(:,:,k+1)=x_new;
    x_past(:,:,1)=[];
    % new dynamic variables
    u_past(:,:,k+1)=u_new;
    u_past(:,:,1)=[];
    
    % new time
    t=[t t_new];
    
    %% plotting on the fly
%     plt=1;
%     limits=4*[-1 1; -1 1; -1 1]; %x-y-z limits
%     view_angle = [1 1 1];
%     skipping = 1;
%     title_text = [flow,' $\alpha = ',num2str(alpha),'$'];
%     plotter_script;
% %     pause
    
    %% Hawking mass 
    H_new = func_V_to_H( u_new(:,4) ,flow,alpha);
    if ~isreal(H_new)
        V_h = u_new(:,4);
        V_h(V_h < 0) = 0;
        H_new = func_V_to_H( V_h ,flow,alpha);
    end
    if strcmp(flow,'iMCF')
        Hawking_mass = [Hawking_mass ( (one_vec.' * M * one_vec) / (16*pi) ).^(0.5) * ( 1 - (H_new.' * M * H_new) / (16*pi) )];
    elseif strcmp(flow,'MCFgen')
        [M,M_m_Halpha] = surface_assembly_P2_Halpha_monotone(x_new,Elements, u_new , flow,alpha);
        m_Halpha = [m_Halpha max( M \ M_m_Halpha )];
    end
    
    %% save data
   % for plotting: the current solution
    s1.t_new = t_new;
    s1.x_new = x_new;
    u_save(:,1:3) = u_new(:,1:3);
    u_save(:,4) = H_new;
    s1.u_new = u_save;
    if strcmp(flow,'iMCF')
        s1.Hawking_mass = Hawking_mass;
    elseif strcmp(flow,'MCFgen')
        s1.m_Halpha = m_Halpha;
    end
    save(['results/MCFgen_test',varargin{:},'/',flow,num2str(alpha),'_solution_at_t',num2str(t_new),'_BDF',num2str(k),'_tau',num2str(tau),'.mat'],'-struct','s1');
    
    % for reruns: saving the past of surfaces and variables as a single 'mat' file
%     s2.t_new = t_new;
%     s2.x_past = x_past;
%     s2.u_past = u_past;
%     save(['results/MCFgen_test',varargin{:},'/',flow,num2str(alpha),'_past','_BDF',num2str(k),'_tau',num2str(tau),'.mat'],'-struct','s2')

end

% disp('')

