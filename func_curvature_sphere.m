function H = func_curvature_sphere(Nodes,R,m)

%% mean curvature (scalar) for any sphere
H = m * (1/R) * ones(size(Nodes,1),1);