function convplots_fig_MCFgeneralised

% Creates convergence plots (ESFEM and ALE ESFEM; M and A norms).
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
format long;

k=2;                % BDF order

% v = - V(H) \nu, with V given as below
% flow = 'MCF';  flow_txt = ''; % MCF: V(H) = H
% flow = 'iMCF';  flow_txt = 'inverse '; % inverse MCF: V(H) = - 1 / H
% flow = 'MCFgen';  flow_txt = 'generalised '; % generalised MCF: V(H) = H^alpha 
flow = 'iMCFgen';  flow_txt = 'generalised inverse '; % generalised inverse MCF: V(H) = - 1 / H^alpha 

alpha = 2;          % flow parameter
if strcmp(flow,'MCF') || strcmp(flow,'iMCF')
    alpha = 1; % setting flow to 1 for mean curvature and inverse mean curvature flows
end


m=2;                % dimension
R0 = 3;
T = 1;              % final time   
T_text = '1';


% spatial refinements
n_vect=(0:4);
% temporal refinements
tau_vect=.2*2.^(-4:-1:-9);

surface = 'Sphere';

if strcmp(surface,'Sphere')
    surface_txt = 'sphere';
    folder = 'MCFgen_test_Sphere'; save_txt = 'Sphere';
else
    disp('surface?')
end


% axis limit and reference line height
factor=10^(-1-k);
ylimits=[10^-5 10^1];



%%
% figure

for i=1:length(n_vect)
    n=n_vect(i);
    Nodes=load(['../surfs/Sphere_nodes_p2_',num2str(n),'.txt']);
%     Elements=load(['../surfs/Sphere_elements_p2_',num2str(n),'.txt']);
%     Elements_plot=load(['../surfs/Sphere_elements_plot_p2_',num2str(n),'.txt']);
    dof=length(Nodes);

    DOF{i}=strcat(['dof ',num2str(dof)]);
    for j=1:length(tau_vect)
        tau=tau_vect(j);
            if exist(['results/',folder,'/errors_',flow,num2str(alpha),'_BDF',num2str(k),'_n',num2str(n),'_tau',num2str(tau),'.txt'], 'file')==2
                err=load(['results/',folder,'/errors_',flow,num2str(alpha),'_BDF',num2str(k),'_n',num2str(n),'_tau',num2str(tau),'.txt']);
                % L^\infty errors on [0,T]
                I=ismember(err(1,:),T);
                Points=(1:length(I));
                T_n = Points(I);
                error_x(i,j)=max(err(2,1:T_n));
                error_nu(i,j)=max(err(3,1:T_n));
                error_H(i,j)=max(err(4,1:T_n));
            else
                error_x(i,j)=NaN;
                error_nu(i,j)=NaN;
                error_H(i,j)=NaN;
            end
    end
end
DOF{i+1}=strcat(['$\mathcal{O}(\tau^',num2str(k),')$']);
% DOF{i+2}=strcat(['$\mathcal{O}(\tau^',num2str(k-1),')$']);

%% plot
f1=figure('position',[10 50 1200 500]);

t1=sgtitle([flow_txt,'mean curvature flow for a ',surface_txt,' in $[0,',num2str(T),']$']);
set(t1,'Interpreter','latex','FontSize',16);
        

subplot(1,3,1)
loglog_conv(tau_vect,error_x)
hold on;
plot(tau_vect,tau_vect.^k/factor,'-. black','LineWidth',1);
% plot(tau_vect,tau_vect.^(k-1)/factor,': black','LineWidth',1);
hold off;
h1=title('$\|X-X_{h}\|_{L^\infty(H^1)}$');
set(h1,'Interpreter','latex','FontSize',18);
h1=xlabel('step size ($\tau$)');
set(h1,'Interpreter','latex','FontSize',16);
h1=ylabel('errors','Interpreter','latex');
set(h1,'Interpreter','latex','FontSize',16);
h2=legend(DOF,'Location','SouthEast','FontSize',10);
set(h2,'Interpreter','latex');
xlim([(1/1.2)*min(tau_vect) 1.2*max(tau_vect)])
ylim(ylimits)

subplot(1,3,2)
loglog_conv(tau_vect,error_nu)
hold on;
plot(tau_vect,tau_vect.^k/factor,'-. black','LineWidth',1);
% plot(tau_vect,tau_vect.^(k-1)/factor,': black','LineWidth',1);
hold off;
h1=title('$\|\nu-\nu_h\|_{L^\infty(H^1)}$');
set(h1,'Interpreter','latex','FontSize',18);
h1=xlabel('step size ($\tau$)');
set(h1,'Interpreter','latex','FontSize',16);
% ylabel('error','Interpreter','latex')
h2=legend(DOF,'Location','SouthEast','FontSize',10);
set(h2,'Interpreter','latex');
xlim([(1/1.2)*min(tau_vect) 1.2*max(tau_vect)])
ylim(ylimits)

subplot(1,3,3)
loglog_conv(tau_vect,error_H)
hold on;
plot(tau_vect,tau_vect.^k/factor,'-. black','LineWidth',1);
% plot(tau_vect,tau_vect.^(k-1)/factor,': black','LineWidth',1);
hold off;
h1=title('$\|H-H_h\|_{L^\infty(H^1)}$');
set(h1,'Interpreter','latex','FontSize',18);
h1=xlabel('step size ($\tau$)');
set(h1,'Interpreter','latex','FontSize',16);
% ylabel('error','Interpreter','latex')
h2=legend(DOF,'Location','SouthEast','FontSize',10);
set(h2,'Interpreter','latex');
xlim([(1/1.2)*min(tau_vect) 1.2*max(tau_vect)])
ylim(ylimits)


% h1=suptitle(['velocity law - ',velocity_law_text]);
% set(h1,'Interpreter','latex','FontSize',18);


saveas(f1,['figures/convplot_',flow,num2str(alpha),'_',save_txt,'_T',T_text,'_BDF',num2str(k),'_time_Linfty.fig']);
saveas(f1,['figures/convplot_',flow,num2str(alpha),'_',save_txt,'_T',T_text,'_BDF',num2str(k),'_time_Linfty.eps'], 'epsc2');


    

function loglog_conv(vect,err)

[n1 n2]=size(err);

symbols='sox.d+^*v><';
ms=[6 6 6 8 6 6 6 6 6 6 6];
gr=(linspace(.66,0,n1))';
colors=[gr gr gr];

for jj=1:n1
    loglog(vect,err(jj,:), ...
           'LineWidth',1,...
           'Marker',symbols(jj),...
           'MarkerSize',ms(jj),...
           'Color', colors(jj,:));%'Color', 'black'); %
    if jj==1
        hold on;
    end
end
hold off;


