function MCFgeneralised_runscript
% 
% Main driver file for 'MCF_generalised' code package.
% 
% Written for the paper: 
% T. Binz and B. Kov�cs. 
% A convergent finite element algorithm for 
% generalized mean curvature flows of closed surfaces.
% IMA Journal of Numerical Analysis, 2021.
% 
% The algorithm for generalised mean curvature flow uses
% quadratic evolving surface finite element in space
% and a BDF method in time.
%
% Required inputs: 
%   - initial surface parametrisation
%   - initial values are based on normal and mean curvature
%   - time interval, stepsizes, meshes, etc.
% 
% Copyright (c) 2021-2022, Bal�zs Kov�cs (balazs.kovacs@ur.de) 
% under the GNU General Public License, version 3.
% 
% If you use 'MCF_generalised' in any program, project, or publication, please acknowledge
% its authors by adding a reference to the above publication.
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

format long;

k=2;                % BDF order

% v = - V(H) \nu, with V given as below
% flow = 'MCF';         % MCF: V(H) = H
% flow = 'iMCF';        % inverse MCF: V(H) = - 1 / H
% flow = 'MCFgen';      % generalised MCF: V(H) = H^alpha 
flow = 'iMCFgen';     % generalised inverse MCF: V(H) = - 1 / H^alpha 

alpha = 2;            % flow parameter

if strcmp(flow,'MCF') || strcmp(flow,'iMCF')
    alpha = 1; % setting flow to 1 for mean curvature and inverse mean curvature flows
end


m=2;                % dimension
R0 = 3;
T = 1;              % final time
check_time(T,R0,m , flow,alpha);
% % tt = 0:0.001:1; plot(tt,func_radius_MCFgeneralised(tt,R0 , flow,alpha));


% spatial refinements
n_vect=(0:4);
% temporal refinements
tau_vect=.2*2.^(-9:-1:-9);

%%%%%%%%%% 
% sphere (max n = 0:7)
surf_name = 'Sphere';
fd=@(x) x(:,1).^2 + x(:,2).^2 + x(:,3).^2 - 1;
% gradient of distance function
grad_fd = @(x) [2 * x(:,1) ...
                2 * x(:,2) ...
                2 * x(:,3)];

save_text = surf_name;


%% adding path of auxiliary functions
addpath('../aux_func');

%% test loops
% loop over mesh sizes
for in=1:length(n_vect)
    n=n_vect(in);
    
    %% loading mesh
    [Nodes,Elements,Elements_plot] = load_num_mesh_p2(surf_name,n,fd,grad_fd);
    % DO NOT scale to any radius
    % the input sphere should be always of radius 1
    
    DOF=length(Nodes)
    
    %% computing initial data
%     nu = func_normal_sphere(Nodes);
%     H = func_curvature_sphere(Nodes,R0,m);
% 
%     u_init = [nu H];
% 
%     DOF=length(Nodes)
    %% loop over timestep sizes
    for jtau=1:length(tau_vect)
        tau=tau_vect(jtau)
        
        MCFgeneralised_solver_test(n,Nodes,Elements,k,tau,m,R0,T , save_text , flow,alpha);
	
    end
end

%% removing path of auxiliary functions
rmpath('../aux_func');


beep
