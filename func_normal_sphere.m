function nrml = func_normal_sphere(Nodes)

%% (outward) unit normal vector for a sphere
nrml = Nodes ./ sqrt( sum(Nodes.^2,2) );