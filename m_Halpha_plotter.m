function m_Halpha_plotter(Nodes,Elements,Elements_plot,k,tau,T0,T, surf_name,title_text, flow,alpha)

% This code plots monotone functions of solutions of generalised (inverse) mean curvature flows.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

format long;

if strcmp(flow,'MCFgen')          % generalised MCF: V(H) =  H^\alpha
    % loading 
    s1=load(['results/MCFgen_test_',surf_name,'/',flow,num2str(alpha),'_solution_at_t',num2str(T),'_BDF',num2str(k),'_tau',num2str(tau),'.mat']);
    x_new=s1.x_new;
    u_new=s1.u_new;
    m_Halpha = s1.m_Halpha;
    
    % plotting
    t = 0:tau:T;
    
    
    f1=figure;
    plot(t,m_Halpha,'o-')
    t1 = title(title_text);
    set(t1,'Interpreter','latex','FontSize',18);
    t1=ylabel('$m_{\textnormal{$H^\alpha$-flow}}$');
    set(t1,'Interpreter','latex','FontSize',16);
    t1=xlabel('time $t$');
    set(t1,'Interpreter','latex','FontSize',15);
    

    saveas(f1,['figures_monotone/m_Halpha_',flow,num2str(alpha),'_',surf_name,'_T',num2str(T),'_tau',num2str(tau),'_BDF',num2str(k),'.fig']);
    saveas(f1,['figures_monotone/m_Halpha_',flow,num2str(alpha),'_',surf_name,'_T',num2str(T),'_tau',num2str(tau),'_BDF',num2str(k),'.png']);
    saveas(f1,['figures_monotone/m_Halpha_',flow,num2str(alpha),'_',surf_name,'_T',num2str(T),'_tau',num2str(tau),'_BDF',num2str(k),'.eps'], 'epsc2');
end


end
        

