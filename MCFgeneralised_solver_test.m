function MCFgeneralised_solver_test(n,Nodes,Elements,k,tau,m,R0,T , save_text , flow,alpha)
%
% This code numerically solves generalised mean curvature flow 
% of a closed surface.
% The code generates data for the convergence plots, generated using
% 'convplots_fig_(*).m'
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
format long;    


%% save initial surface
% degrees of freedom
dof=length(Nodes);

% saving spherical mesh of radius 1
Nodes0=Nodes;

%% BDF coefficients
[delta,gamma]=BDF_tableau(k);

%% structure of variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% surface:
% --> the tensor x (dimension: dof \times 3 \times T/tau) stores 
%     the surface nodes at time t_k in its k-th layer x(:,:,k)
% --> the matrix x(:,:,k) (dimension: dof \times 3) stores the nodes of the
%     discrete surface (coordinates are column-wise, nodes row-wise)
% 
% dynamic variables:
% --> the tensor u (dimension: dof \times 4 \times T/tau) stores
%     the nodeal values of the dynamic variables (n_h,H_h) at time t_k
%     in its k-th layer u(:,:,k)
% --> the matrix u(:,:,k) (dimension: dof \times 4) stores 
%     the nodal values of n_h in its first three columns, and
%     the nodal values of H_h in its fourth column
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% initial values
t=[];
for ind=0:k-1
    t_new=ind*tau;
    t=[t t_new];
    % exact radius for generalised MCF for sphere
    Radius = func_radius_MCFgeneralised(t_new,R0 , flow,alpha);
%     R(1,ind+1) = Radius;
    x(:,:,ind+1)=Radius*Nodes0;
    u(:,:,ind+1)=[func_normal_sphere(x(:,:,ind+1)) func_H_to_V( func_curvature_sphere(x(:,:,ind+1),Radius,m) , flow,alpha)];
end

x_past=x;
u_past=u;

%% time Integration
for steps=k:ceil(T/tau)
    % timestep
    t_new=t(end)+tau;
    
    % extrapolations
%     R_tilde = 0;
    x_tilde=zeros(dof,3);
    u_tilde=zeros(dof,4);
    for ind=1:k
        % radius 
%         R_tilde = R_tilde + gamma(k-ind+1)*R(1,end-k+ind);
        % surface
        x_tilde=x_tilde+gamma(k-ind+1)*x_past(:,:,end-k+ind);
        % dynamic variables u = (nu,V)
        u_tilde=u_tilde+gamma(k-ind+1)*u_past(:,:,end-k+ind);
    end
    
    %%%%%%%%
    % projecting nu_h onto the unit sphere
    u_tilde(:,1:3) = u_tilde(:,1:3) ./ sqrt( sum(u_tilde(:,1:3).^2,2) );
    %%%%%%%%
    
    %%%%%%%%
    % surface FEM assembly on the EXTRAPOLATED surface
    [M,A,Mxu,f,g1,g2] = surface_assembly_P2_MCFgeneralised(x_tilde,Elements, u_tilde , flow,alpha);
    
% %     Radius = func_radius_MCFgeneralised(t_new,R0 , flow,alpha);
% %     x_tilde = Radius*Nodes0;
% %     u_tilde = [func_normal_sphere(x_tilde) func_H_to_V( func_curvature_sphere(x_tilde,Radius,m) , flow,alpha)];
% %     [M,A,Mxu,f,g1,g2] = surface_assembly_P2_MCFgeneralised(x_tilde,Elements, u_tilde , flow,alpha);
    
    % the K matrix from the H^1 inner product
    K = M + A;
    % g -- r.h.s. of the velocity law
    g = - g1 - g2;
     
    % contributions from the past
    % rho_v
%     dR = 0;
    dx=0*x_tilde;
    du=0*u_tilde;
    for ind = 1:k
%         dR = dR + delta(ind+1) * R(1,end-ind+1);
        dx = dx + delta(ind+1) * x_past(:,:,end-ind+1);
        du = du + delta(ind+1) * u_past(:,:,end-ind+1);
    end
    rho_v = tau * g - K * dx;
    rho_u = tau * f - Mxu * du;
    
    % solving the 3 + 4 decoupled linear systems
    % velocity law
    x_new = (delta(1)*K) \ rho_v;
    % dynamic variables
    u_new = ( delta(1)*Mxu + tau*A ) \ rho_u;
    %%%%%%%%
    
%     R = [R, delta(1)\(tau*R_tilde/m - dR)];
    
    %%%%%%%%
    % projecting nu_h onto the unit sphere
%     u_new = u_new ./ sqrt( sum(u_new.^2,2) );
    %%%%%%%%
    
    %%%%%%%%
    % updating the solutions and the time vector
    % new surface
    x_past(:,:,k+1)=x_new;
    x_past(:,:,1)=[];
    % new dynamic variables
    u_past(:,:,k+1)=u_new;
    u_past(:,:,1)=[];
    
    % new time
    t=[t t_new];
    
    %% error (??)
    % exact radius for generalised MCF for sphere
    Radius = func_radius_MCFgeneralised(t_new,R0 , flow,alpha);
    % the exact solution
    x_MCFgen = Radius*Nodes0;
    u_MCFgen = [func_normal_sphere(x_MCFgen) func_curvature_sphere(x_MCFgen,Radius,m)];
    
    e_x = x_new - x_MCFgen;
    e_nu = u_new(:,1:3) - u_MCFgen(:,1:3);
    H_new = func_V_to_H( u_new(:,4) ,flow,alpha);
    e_H = H_new - u_MCFgen(:,4);
    errors(1,1:steps+1)=t;
    errors(2,steps+1)=sqrt(e_x(:,1)'*K*e_x(:,1) + e_x(:,2)'*K*e_x(:,2) + e_x(:,3)'*K*e_x(:,3));
    errors(3,steps+1)=sqrt(e_nu(:,1)'*K*e_nu(:,1) + e_nu(:,2)'*K*e_nu(:,2) + e_nu(:,3)'*K*e_nu(:,3));
    errors(4,steps+1)=sqrt(e_H'*K*e_H);
    
    % errors as 'txt' files
    save(['results/MCFgen_test_',save_text,'/errors_',flow,num2str(alpha),'_BDF',num2str(k),'_n',num2str(n),'_tau',num2str(tau),'.txt'], 'errors', '-ASCII');
    
    %% save data
%     % for plotting: the current solution
%     s1.t_new=t_new;
%     s1.x_new=x_new;
%     s1.u_new=u_new;
%     save(['results/MCFgen_test_',save_text,'/solution_',flow,num2str(alpha),'_at_t',num2str(t_new),'_BDF',num2str(k),'_n',num2str(n),'_tau',num2str(tau),'.mat'],'-struct','s1');
    
    % for reruns: saving the past of surfaces and variables as a single 'mat' file
%     s2.t_new=t_new;
%     s2.x_past=x_past;
%     s2.u_past=u_past;
%     save(['results/MCFgen_test_',save_text,'/past_',flow,num2str(alpha),'','_BDF',num2str(k),'_n',num2str(n),'_tau',num2str(tau),'.mat'],'-struct','s2')

end
% disp('')

